This is a boilerplate for a simple SPA web app using vanilla js.
This project is able to use ES6 import in browser with the help of type=module in script tag
To know more about it visit [here]('https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import') & [here]('https://v8.dev/features/modules')